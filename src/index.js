import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/common/Header';
import Top from './components/sections/Top'
import About from './components/sections/About';
import Brand from './components/sections/Brand';
import Footer from './components/common/Footer';
import './font/font.scss'
import 'bulma/css/bulma.min.css';
import './style/general.scss'

ReactDOM.render(
  <>
    <Header/>
    <Top/>
    <About/>
    <Brand/>
    <Footer/>
  </>,
  document.getElementById('root')
);
