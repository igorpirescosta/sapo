import React from "react"
import './footer.scss'
import ReactLogo from '../../../images/redes.png'

const Footer = () => {
  return (
    <section className="Footer">
      <img src={ReactLogo} alt="Redes Sociais" />
    </section>
  )
}

export default Footer