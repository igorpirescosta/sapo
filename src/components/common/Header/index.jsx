import React from "react";
import ReactLogo from '../../../images/sapo_logo.svg'

// css
import './header.scss'


const links = [
  {
    name:'LOREM',
    href:'#'
  },{
    name: 'IPSUM',
    href: '#'
  },{
    name: 'DOLOR',
    href: '#'
  }
]

const Header = () => {

  const [isActive, setActive] = React.useState(false)

  function handleMenu(){
    setActive(!isActive)
    // 
    if(!isActive){
      document.body.classList.add('menuActive')
    }else {
      document.body.classList.remove('menuActive')
    }
    
  }
  
  return(
    <header className="is-full">
      <div className="contentMain">

        <nav className="navbar" role="navigation" aria-label="main navigation">
          <a href='/' className=" navbar-brand logo" title='SAPO'>
            <img src={ReactLogo} alt="SAPO" />
          </a>
          <button className={isActive ? "is-active navbar-burger" : "navbar-burger"} data-target="navMenu" aria-label="menu" aria-expanded="false" onClick={handleMenu}>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </button>
          <div className={isActive ? "navbar-menu active" : "navbar-menu"}>
            <ul className='navbar-start'>
              {links.map((item, index) => (
                <li key={index} className='navbar-item'>
                  <a href={item.href} title={item.name}>{item.name}</a>
                </li>
              ))}
            </ul>
          </div>
        </nav>
      </div>
    </header>
  )
}

export default Header