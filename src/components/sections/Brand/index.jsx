import React from "react"
import './Brand.scss'

const Brand = () => {
  // 
  const [isJump, setJump] = React.useState(0)
  const [isX, setX] = React.useState('')
  const [isY, setY] = React.useState('')
  const [isD, setD] = React.useState('')
  const [isResp, setResp] = React.useState('')

  const handleJump = (e) => {
    switch (e.target.name) {
      case 'x':
        setX(e.target.value)
        break;
      case 'y':
        setY(e.target.value)
        break;
      case 'd':
        setD(e.target.value)
        break;
      default:
        break;
    }
  }
  const validate = () => {
    
    let resp = false
    
    if((document.body.classList[0] === undefined || document.body.classList[0] === 'green') && Number(isX) <= Number(isY)){
      setX(3)
      resp = true
    } 
    
    if((document.body.classList[0] === 'blue' && Number(isX) <= 7) && Number(isX) <= Number(isY)) {
      resp = true
    }
    
    if(Number(isX) <= Number(isY) && document.body.classList[0] === 'orange'){
      resp = true
    }

    return resp
  }

  const handleTotal = () => {
    // 
    document.getElementById('frog').classList.remove('jumping')
    if(validate()){
      setResp('')
      const final = Number(isY)
      let count = 0
      let sum = 0
      for (count; sum <= final; count++) {
        sum = Number(isX) + (count * Number(isD))
      }
      setJump(count - 1)
      document.getElementById('frog').classList.add('jumping')
    }else {
      setResp('Números não se encaixam nas regras, favor revisá-los')
    }
  }

  return(
    <section className="brand">
      
      <div className="contentSkewY">
        <div className="contentNormal">
          <div className="contentMain">
          
            <div className="title">
              <h1>Deu {isJump} {isJump > 1 ? 'saltos' : 'salto'}</h1>
            </div>
            <div className="coordinates">
              <label htmlFor="x">
                X
                <input type="number" name="x" id="x" value={isX} onChange={handleJump}/>
              </label>
              <label htmlFor="y">
                Y
                <input type="number" name="y" id="y" value={isY} onChange={handleJump}/>
              </label>
              <label htmlFor="d">
                D
                <input type="number" name="d" id="d" value={isD} onChange={handleJump}/>
              </label>
              <button onClick={handleTotal}>Dá o salto</button>
              <p>{isResp}</p>
            </div>
            <div className="jumperFrog">
              <div id='frog' className="frog"></div>
              <div className="goal"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Brand