import React from "react"
import './About.scss'

const About = () => {
  return(
    <section className="about">
      <div className="contentMain">
        <div className="title">
          <h1>Sapo</h1>
        </div>
        <div className="text">
          <p>
            Estamos consigo desde 1995, mas ainda mal começámos a tocar o futuro
          </p>
          <p>
            Apostamos na credibilidade, na inovação, na tecnologia e na proximidade
          </p>
          <p>
            Somos o SAPO, vamos tornar a sua vida mais simples, mais informada, mais divertida, mais prática, aqui e em todo o mundo. Queremos estar na sua vida.
          </p>
        </div>
        <div className="links">
          <a href="http://videos.sapo.pt/bRi3wOIOTyf2cWDtNkpj?jwsource-cl" target='_blank' rel="noreferrer" title="Manifesto">Manifesto</a>
          <a href="#marca" title="O SAPO deu o salto">O SAPO deu o salto</a>
        </div>
      </div>
    </section>
  )
}

export default About