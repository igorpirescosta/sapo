import React from "react"
import ReactLogo from '../../../images/SAPO.svg'
import './Top.scss'

const Top = () => {

  const [isColor, setColor] = React.useState("green")
  const [isOrder, setOrder] = React.useState(['orange','green', 'blue'])

  function handleOrder(color, order) {
    setColor(color)
    setOrder(order)
    document.body.classList.remove(isColor)
    document.body.classList.add(color)
  }
  
  return(
    <section className="top">
      

        <div className="geometric">
          <div className={"rectangle rectangle-one border-"+isColor}></div>
          <div className={"triangle triangle-one border-"+isColor}></div>
          <div className={"rectangle rectangle-two border-"+isColor}></div>
          <div className={"circle circle-one border-"+isColor}></div>
          <div className={"triangle triangle-two border-"+isColor}></div>
          <div className={"circle circle-two border-"+isColor}></div>
        </div>

        <div className="frog">
          <img src={ReactLogo} alt="SAPO" />
        </div>

        <div className="thumbs columns is-mobile">
          {isOrder.map((order, index) => {
            let list = []
            switch (order) {
              case 'orange':
                list = ['green','orange','blue']
                break;
              case 'blue':
                list = ['orange','blue','green']
                break
              default:
                list = ['orange','green', 'blue']
            }
            return(
                <span key={index} className={order + ' column order'+index} onClick={() => handleOrder(order, list)}></span>
            )    
          })}
          
        </div>
      
    </section>
  )
}


export default Top
