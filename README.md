# SAPO - Exercício técnico

Projeto realizado como exercício técnico para o SAPO. Utilizando React, SASS, bulma e Yarn

## Passo a passo

Após clonar o repositório e entrar na pasta do projeto para rodar o projeto local basta primeiro:

### `yarn install`

Irá instalar todas as dependências do projeto. Após a instalação:

### `yarn start`

Irá rodar o projeto localmente, em modo desenvolvimento.\
Automaticamente abrirá em uma aba do navegador o localhost [http://localhost:3000](http://localhost:3000)\
É possível acessar o projeto local no dispositivo móvel [http://192.168.0.171:3000](http://192.168.0.171:3000). 
Apenas é necessário que o computador e o dispositivo móvel estejam na mesma rede.

## Gerando Build

Para gerar o build da aplicação basta acionar o comando:
### `yarn build`

O código final será gerado na pasta /build